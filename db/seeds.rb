Product.create(
  title: 'Hawaiian',
  description: 'This is Hawaiian pizza',
  price: 350,
  size: 30,
  is_spicy: false,
  is_veg: false,
  is_best_offer: false,
  path_to_image: '/images/hawaiian.jpg')

Product.create(
  title: 'Diablo',
  description: 'This is Diablo pizza',
  price: 500,
  size: 30,
  is_spicy: true,
  is_veg: false,
  is_best_offer: false,
  path_to_image: '/images/diablo.jpg')

Product.create(
  title: 'Veg',
  description: 'This is Veg pizza',
  price: 650,
  size: 30,
  is_spicy: false,
  is_veg: true,
  is_best_offer: false,
  path_to_image: '/images/veg.jpg')

Product.create(
  title: 'Bambini',
  description: 'This is Bambini pizza',
  price: 450,
  size: 30,
  is_spicy: false,
  is_veg: false,
  is_best_offer: false,
  path_to_image: '/images/Bambini.jpg')

Product.create(
  title: 'Ispana',
  description: 'This is Ispana pizza',
  price: 550, size: 30, is_spicy: false,
  is_veg: false,
  is_best_offer: false,
  path_to_image: '/images/Ispana.jpg')

Product.create(
  title: 'Quadro',
  description: 'This is Quadro pizza',
  price: 450,
  size: 30,
  is_spicy: false,
  is_veg: true,
  is_best_offer: false,
  path_to_image: '/images/Quadro.jpg')

Product.create(
  title: 'Sicilia',
  description: 'This is Sicilia pizza',
  price: 750,
  size: 30,
  is_spicy: false,
  is_veg: false,
  is_best_offer: false,
  path_to_image: '/images/Sicilia.jpg')

Product.create(
  title: 'Verona',
  description: 'This is Verona pizza',
  price: 650,
  size: 30,
  is_spicy: false,
  is_veg: false,
  is_best_offer: false,
  path_to_image: '/images/Verona.jpg')

Product.create(
  title: 'Spinaci',
  description: 'This is Spinaci pizza',
  price: 550,
  size: 30,
  is_spicy: false,
  is_veg: true,
  is_best_offer: false,
  path_to_image: '/images/Spinaci.jpg')