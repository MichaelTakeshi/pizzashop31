require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'pry'
require 'sqlite3'
require 'sinatra/activerecord'

set :database, "sqlite3:pizzashop.db"

class Product < ActiveRecord::Base
end

class Order < ActiveRecord::Base
end

before do 
  @products = Product.all
  @orders = Order.all
end

get '/' do 
  @products = Product.all
  @orders = Order.all
  erb :index
end

get '/about' do 
  erb :about
end

post '/place_order' do
  @order = Order.create params[:order]
  erb :order_placed
end

get '/admin' do
  erb :admin
end


post '/cart' do

  #получаємо список параметрів та парсемо
  @orders_input = params[:orders]
  @items = parse_orders(@orders_input)

  return erb :cart_is_empty if @items.length.zero?

  @items.map! do |item|
    { product: Product.find(item.first), qty: item.last }
  end

  erb :cart
end

def parse_orders(orders_input)

  s1 = orders_input.split(/,/)
  arr = []
  s1.each do |x|
    s2 = x.split(/=/)
    s3 = s2[0].split(/_/)
    id = s3[1]
    cnt = s2[1]
    arr2 = [id, cnt]
    arr.push arr2
  end
  arr
end